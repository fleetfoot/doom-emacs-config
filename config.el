;;; config.el --- -*- lexical-binding: t -*-

(setq user-full-name "Jeff Temes"
      user-mail-address "hello@jefftemes.com"
      epa-file-encrypt-to user-mail-address)

(setq doom-theme 'doom-one)

(setq display-line-numbers-type nil)

(setq-default indent-tabs-mode nil)

(setq projectile-project-search-path '("~/work/"
                                       "~/projects/"
                                       "~/Documents/Dropbox/Roam"
                                       "~/Documents/Dropbox/Org"))

(setq auto-save-default t
      make-backup-files t)

;;They're generally unhelpful and only add confusing visual clutter.
(setq mode-line-default-help-echo nil)

(add-to-list 'auto-mode-alist '("\\.njk?\\'" . web-mode))

(setq show-help-function nil)

(custom-set-faces!
  '((hl-line solaire-hl-line-face org-indent) :extend t))

(global-visual-line-mode t)

(add-hook 'js2-mode-hook #'format-all-mode)

(setenv "PATH" (concat (getenv "PATH") ":/home/fleet/.volta/bin"))
(setq exec-path (append exec-path '("/home/fleet/.volta/bin")))

(eval-after-load 'js-mode
  '(add-hook 'js-mode-hook #'add-node-modules-path))

(setq js-indent-level 2)

(set-window-margins (selected-window) 20 20)

(add-hook! '+popup-buffer-mode-hook
  (set-window-margins (selected-window) 20 20))

(ignore-errors
  (setq doom-font (font-spec :family "DejaVu Sans Mono" :size 16)
        doom-big-font (font-spec :family "DejaVu Sans Mono" :size 16)
        doom-variable-pitch-font (font-spec :family "Overpass" :size 16)
        doom-serif-font (font-spec :family "IBM Plex Mono")))

(setq-default line-spacing 0.2)

(after! writeroom-mode
  (setq +zen-text-scale 0.6))

(setq doom-modeline-height 1)
(set-face-attribute 'mode-line nil :height 109)
(set-face-attribute 'mode-line-inactive nil :height 109)

(setq doom-themes-treemacs-enable-variable-pitch nil)

(defun shell-hook ()
   (text-scale-decrease 2.5))

(add-hook 'term-mode-hook 'shell-hook)

(add-hook! 'org-mode-hook #'mixed-pitch-mode)
(setq mixed-pitch-variable-pitch-cursor nil)

(setq initial-frame-alist '((top . 1) (left . 1) (width . 75) (height . 55)))



(+global-word-wrap-mode +1)

(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))

(add-to-list 'auto-mode-alist '("\\.liquid$" . html-mode))

(setq org-cycle-separator-lines -1)

(setq org-roam-directory "~/Documents/Dropbox/Roam")

(after! org
  (setq org-agenda-files '("~/Documents/Dropbox/Org/Main/inbox.org"
                           "~/Documents/Dropbox/Org/Main/todo.org"
                           "~/Documents/Dropbox/Org/SFU/Spring2021/MACM101.org"
                           "~/Documents/Dropbox/Org/SFU/Spring2021/MATH100.org"
                           "~/Documents/Dropbox/Org/SFU/Spring2021/ENGL111W.org"
                           "~/Documents/Dropbox/Org/SFU/Spring2021/BISC113.org"
                           "~/Documents/Dropbox/Org/Main/tickler.org")))

(after! org
  (setq org-refile-targets '((org-agenda-files :level . 1)
                             ("~/Documents/Dropbox/Org/Main/idea.org" :level . 1)
                             ("~/Documents/Dropbox/Org/Main/bill.org" :level . 1)
                             ("~/Documents/Dropbox/Org/Main/notes.org" :level . 1)
                             ("~/Documents/Dropbox/Org/Main/todo.org" :level . 1)
                             ( "~/Documents/Dropbox/Org/SFU/Spring2021/MACM101.org" :level . 1)
                             ("~/Documents/Dropbox/Org/SFU/Spring2021/MATH100.org" :level . 1)
                             ("~/Documents/Dropbox/Org/SFU/Spring2021/ENGL111W.org" :level . 1)
                             ("~/Documents/Dropbox/Org/SFU/Spring2021/BISC113.org" :level . 1))))

(after! org
  (advice-add 'org-refile :after 'org-save-all-org-buffers)
  (setq org-agenda-skip-scheduled-if-done t
        org-startup-truncated t
        org-startup-folded t
        org-directory "~/Documents/Dropbox/Org/"
        org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "|" "BILL(b)" "DONE(d)")
                            (sequence
                             "WAIT(w)" ; waiting for some external change (event)
                             ;; "HOLD(h)" ; waiting for some internal change (of mind)
                             "IDEA(i)" ; maybe someday
                             "|"
                             "STOP(s@/!)" ; stopped waiting, decided not to work on it
                             ))
        org-ctrl-k-protect-subtree t
        org-log-done 'time))

(after! org-capture
  (setq org-capture-templates '(
                                ("i" "Inbox" entry
                                 (file+headline "~/Documents/Dropbox/Org/Main/inbox.org" "Inbox")
                                 "* TODO %i%?")

                                ("b" "BISC113 Anki basic"
                                 entry
                                 (file+headline "~/Documents/Dropbox/Org/SFU/Spring2021/BISC113/anki.org" "Dispatch")
                                 "* %<%H:%M>\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Basic\n:ANKI_DECK: Mega\n:END:\n** Front\n%?\n** Back\n")

                                ("B" "BISC113 Anki cloze"
                                 entry
                                 (file+headline "~/Documents/Dropbox/Org/SFU/Spring2021/BISC113/anki.org" "Dispatch")
                                 "* %<%H:%M>\n:PROPERTIES:\n:ANKI_NOTE_TYPE: Cloze\n:ANKI_DECK: Mega\n:END:\n** Text\n%?\n** Extra\n")
                                )))

(defun diary-last-day-of-month (date)
  "Return `t` if DATE is the last day of the month."
  (let* ((day (calendar-extract-day date))
         (month (calendar-extract-month date))
         (year (calendar-extract-year date))
         (last-day-of-month
          (calendar-last-day-of-month month year)))
    (= day last-day-of-month)))

(setq deft-directory "~/Documents/Dropbox/Org"
      deft-extensions '("org")
      deft-recursive t)

(setq  org-journal-date-prefix "#+TITLE:"
       org-journal-time-prefix "* "
       org-journal-file-format "%Y-%m-%d.org")

(setq org-archive-location
      "~/Documents/Dropbox/Org/Main/archives/archive.org::datetree/")

(after! org (setq org-hide-emphasis-markers t))

(after! org
  (setq org-log-done t)
  (setq org-log-into-drawer t))

(add-hook! org-mode (electric-indent-local-mode -1))

(after! org-agenda
  ;; (setq org-agenda-prefix-format
  ;;       '((agenda . " %i %-12:c%?-12t% s")
  ;;         ;; Indent todo items by level to show nesting
  ;;         (todo . " %i %-12:c%l")
  ;;         (tags . " %i %-12:c")
  ;;         (search . " %i %-12:c")))
  ;; (setq org-agenda-include-diary t)

  (setq org-agenda-prefix-format
        (quote
         ((agenda . "%-12c%?-12t% s")
          (timeline . "% s")
          (todo . "%-12c")
          (tags . "%-12c")
          (search . "%-12c"))))


  (setq org-agenda-deadline-leaders (quote ("!D!: " "D%2d: " "")))

  (setq org-agenda-scheduled-leaders (quote ("" "S%3d: ")))
  )

(after! org
  (setq org-blank-before-new-entry
        '((heading . always)
         (plain-list-item . always))))

(after! org-pomodoro
  (setq org-pomodoro-format "%s"))

(remove-hook! 'text-mode-hook #'spell-fu-mode)

(setq ispell-dictionary "en")

;; (require 'org-gcal)
;; (setq org-gcal-client-id "71537342966-4cv9l1qk08irsgerd9i3j5611r20ummv.apps.googleusercontent.com"
;;       org-gcal-client-secret "qkY3tUJs_qOeyQkqGXGIxHJE"
;;       org-gcal-fetch-file-alist '(("jefftemes@gmail.com" .  "~/Documents/Dropbox/Org/Main/todo.org")

;; (require 'org-gtasks)
;; (org-gtasks-register-account :name "Jeff"
;;                              :directory "~/Documents/Dropbox/Org/Main/"
;;                              :client-id "71537342966-4cv9l1qk08irsgerd9i3j5611r20ummv.apps.googleusercontent.com"
;;                              :client-secret "qkY3tUJs_qOeyQkqGXGIxHJE")

(add-to-list 'auto-mode-alist '("\\.cl\\'" . lisp-mode))

(setq lsp-enable-file-watchers nil)

(setq +latex-viewers '(pdf-tools evince zathura okular skim sumatrapdf))

(map!
 :map LaTeX-mode-map
 :localleader
 :desc "View" "v" #'TeX-view)

(global-set-key (kbd "C-O") 'better-jumper-jump-forward)

(map! :map org-mode-map
      :localleader
      (:prefix ("l" . "latex")
        "p" #'org-latex-preview))

(after! ledger-mode
  (setq ledger-use-iso-dates t))

(require 'eglot)

(add-to-list 'eglot-server-programs
             '(scss-mode . ("css-languageserver" "--stdio")))
(add-hook 'scss-mode-hook 'eglot-ensure)

(add-to-list 'eglot-server-programs
             '(web-mode . ("html-languageserver" "--stdio")))
(add-hook 'web-mode-hook 'eglot-ensure)

(add-to-list 'eglot-server-programs
             '(php-mode . ("php" "/home/fleet/.config/composer/vendor/felixfbecker/language-server/bin/php-language-server.php")))
(add-hook 'php-mode-hook 'eglot-ensure)

(add-to-list 'eglot-server-programs
             '(latex-mode . ("texlab")))
(add-hook 'latex-mode-hook 'eglot-ensure)

;; (setf (alist-get t ivy-posframe-display-functions-alist)
;;       'ivy-posframe-display-at-frame-bottom-left)

;; (setf (alist-get 'ivy-completion-in-region ivy-posframe-display-functions-alist)
;;       'ivy-posframe-display-at-point)

;; (setq ivy-posframe-parameters '((left-fringe . 0)
;;                                 (right-fringe . 0)
;;                                 (internal-border-width . 1)))



;; (setq ivy-height 10)
;; (setq ivy-posframe-height ivy-height)

;; (setq ivy-posframe-size-function
;;       (defun ivy-posframe-get-size+ ()
;;         (if (eq ivy--display-function
;;                 'ivy-posframe-display-at-point)
;;             (list
;;              :min-height ivy-posframe-height
;;              :min-width 80)
;;           (list
;;            :min-height ivy-posframe-height
;;            :min-width (+ 2 (frame-width))))))

(use-package! anki-editor
  :defer 10
  :after org
  :bind (:map org-mode-map
         ("<f12>" . anki-editor-cloze-region-dont-incr)
         ("<f11>" . anki-editor-cloze-region-auto-incr)
         ("<f10>" . anki-editor-reset-cloze-number)
         ("<f9>"  . anki-editor-push-tree))
  :hook (org-capture-after-finalize . anki-editor-reset-cloze-number) ; Reset cloze-number after each capture.
  :config
  (setq anki-editor-create-decks t
        anki-editor-org-tags-as-anki-tags t)

  (defun anki-editor-cloze-region-auto-incr (&optional arg)
    "Cloze region without hint and increase card number."
    (interactive)
    (anki-editor-cloze-region my-anki-editor-cloze-number "")
    (setq my-anki-editor-cloze-number (1+ my-anki-editor-cloze-number))
    (forward-sexp))
  (defun anki-editor-cloze-region-dont-incr (&optional arg)
    "Cloze region without hint using the previous card number."
    (interactive)
    (anki-editor-cloze-region (1- my-anki-editor-cloze-number) "")
    (forward-sexp))
  (defun anki-editor-reset-cloze-number (&optional arg)
    "Reset cloze number to ARG or 1"
    (interactive)
    (setq my-anki-editor-cloze-number (or arg 1)))
  (defun anki-editor-push-tree ()
    "Push all notes under a tree."
    (interactive)
    (anki-editor-push-notes '(4))
    (anki-editor-reset-cloze-number))
  ;; Initialize
  (anki-editor-reset-cloze-number)
  )
